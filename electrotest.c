#include <stdio.h>
#include <stdlib.h>
#include "lib.h"

float effekt(float resistans, float volt){

	char val;
	float effekt;
	float strom;

    printf("Välj funktion:\n(1) volt^2 / resistans\n(2) volt * ström\n(0) Exit\nVal:");
    do{
        scanf(" %c", &val);
        if(!((val=='0')||(val=='1')||(val=='2')))
            printf("Fel inmatning. Mata in 0-2\n");
    }
    while(!(val=='0'||val=='1'||val=='2'));

	switch(val){
		case '1':
			effekt = calc_power_r(volt, resistans);
			break;
		case '2' :
			printf("Mata in ström:");
			scanf("%f", &strom);
			effekt = calc_power_i(volt, strom);
			break;
		default:
			exit(0);
	}
	
    return effekt;	
		
}

void visaErsRes(int count, float array[]){
	int i;
	printf("Ersättningsresistanser i E12-serien kopplade i serie:");
	for(i=0;i<count;i++){
		printf(" %f", array[i]);
	}
}

void setResistors(int count, float array []){

	int i;
	for(i=0;i<count;i++){
		printf("Komponent %d i ohm: ",i+1);
		scanf("%f",&(array[i]));
	}
}

int antal(){

    int num;
    printf("Ange antal resistorer: ");
    scanf("%d", &num);

    return num;

    }

char koppling(){

    char conn;

    printf("Ange koppling [S|P]: ");
    do{
        scanf(" %c", &conn);
        if(!((conn=='P')||(conn=='S')))
            printf("Fel inmatning. Mata in [S|P]\n");
    }
    while(!(conn=='P'||conn=='S'));

    return conn;
}

float spanning(){
	float volt;
	
	printf("Ange spänningskälla i V: ");
	
	do{
        scanf("%f", &volt);
        if(volt<0)
            printf("Fel inmatning. Värdet måste vara större än 0");
    }
    while(volt<0);

    return volt;
}

int main(){

	float volt = spanning(), ersRes;
    char koppl = koppling();
    int num = antal();
    float* resArray = (float *)malloc(sizeof(float)*num);
    if(num>0){
		float* e12Res = malloc(sizeof(int)*3);
		setResistors(num,resArray);
		ersRes=calc_resistance(num,koppl,resArray);
		printf("Ersättningsresistans: %.2f ohm\n",ersRes);
		printf("Effekt: %.2f W\n", effekt(ersRes, volt));
		
		int count = e_resistance(ersRes, e12Res);
		
		printf("Ersättningsresistanser i E12 serien kopplade i serie:");
		int h;		
		for(h=0;h<count;h++){
			printf(" %.1f",e12Res[h] );		
		}		
		printf("\n");
		free(e12Res);
	}
	free(resArray);
	
		
	return 0;
}
