#include <stdio.h>
#include "lib.h"

float calc_resistance(int count, char conn, float * array){

	float sum=0;

	if(count>0){
		int i;
		if(conn=='P'){
			for(i=0;i<count;i++)
				sum+=(1/array[i]);
			sum=1/sum;
		}
		else if(conn=='S'){
			for(i=0;i<count;i++)
				sum+=array[i];
		}	
	}
	
	return sum;
	
}
