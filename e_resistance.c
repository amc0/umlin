#include <stdio.h>
#include <stdlib.h>
#include "lib.h"

int e_resistance(float orig_resistance, float *res_array ){
    float Saved_orig_resistance = orig_resistance;
    float E12[12] = {10.0, 12.0, 15.0, 18.0, 22.0, 27.0, 33.0, 39.0, 47.0, 56.0, 68.0, 82.0};
    float Rs[3] = {0.0, 0.0, 0.0};
    int ret = 0;
    int SmartValu = 0;
    int MinRest = orig_resistance;
    while(orig_resistance != 0 && SmartValu != 10){
        orig_resistance = Saved_orig_resistance;
        int j = 0;
        float Rt[3] = {0.0, 0.0, 0.0};
        while(j < 3){
            int i;
            int multiple_Size = 0;
            int Evalue = 0;
            for (i = 11; i >= SmartValu; i--){
                int current_test = E12[i];
                int times = 0;
                while ((current_test * (10*times) ) <= orig_resistance){
                    if (10*times == 0){
                        if(current_test <= orig_resistance){
                            times ++;
                        }else{
                            break;
                        }
                    }else{
                        times ++;
                    }
                }
                if(times > multiple_Size){
                    multiple_Size = times;
                    Evalue = i;
                }
            }
            if (multiple_Size > 1){
                Rt[j] = (E12[Evalue] * (float)(10*(multiple_Size-1)));
            }else if(orig_resistance > E12[0] && multiple_Size <= 1){
                Rt[j] = (E12[Evalue]);
            }else{
                Rt[j] = 0.0;
            }
            orig_resistance -= Rt[j];
            j++;
        }
        if(orig_resistance < MinRest){
            MinRest = orig_resistance;
            Rs[0] = Rt[0];
            Rs[1] = Rt[1];
            Rs[2] = Rt[2];
        }
        SmartValu ++;
    }
    if (MinRest > 0){
        float RemainderPros = (((float)MinRest/Saved_orig_resistance)*100.0);
        printf("the difference is: %.2f %% of the given reference resistance\n", RemainderPros);
    }
    int i;
    for (i = 0; i <= 2; i++){
        if(Rs[i] > 0.0){
            ret ++;
        }
    }
    *(res_array) = Rs[0];
    *(res_array+1) = Rs[1];
    *(res_array+2) = Rs[2];
    return ret;
}
