#include "lib.h"
//function for calculating power (volt^2/resistance)
float calc_power_r(float volt, float resistance){
		
	float power = (volt * volt) / resistance;
			
	return power;
	}
