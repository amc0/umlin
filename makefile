### FLAGS AND VARIABLES ###

CC=gcc
CFLAGS=-c -fPIC
SHRDCFLAG= -shared -fPIC -o

INSTALLDIR='/usr/bin'
LIBDIR='/usr/lib'
LCLLIBDIR='./lib/'
PRG=electrotest

LIBFILES = libresistance.so libpower.so libcomponent.so
LIBCFILES= resistance.c powerI.c powerR.c e_resistance.c
LIBSOFILES = -lresistance -lpower -lcomponent

### MAKE COMMANDS ###

all: program

program: lib lib.h
	@echo Creating $(PRG)
	$(CC) -L$(LCLLIBDIR) -Wl,-rpath=$(LCLLIBDIR) -o $(PRG) $(PRG).c $(LIBSOFILES)

lib: src $(LIBFILES)
	mkdir lib
	mv *.so $(LCLLIBDIR)

libresistance.so:
	$(CC) $(SHRDCFLAG) $@ resistance.o

libpower.so:
	$(CC) $(SHRDCFLAG) $@ powerR.o powerI.o

libcomponent.so:
	$(CC) $(SHRDCFLAG) $@ e_resistance.o

src:
	@echo Assembling libraries
	$(CC) $(CFLAGS) $(LIBCFILES)
	

install: all
	@echo Installing $(PRG) to $(INSTALLDIR)
	mv ./$(PRG) $(INSTALLDIR)
	@echo Installing libraries to $(LIBDIR)
	mv $(LCLLIBDIR)*.so $(LIBDIR)

clean:
	rm *.o
	rm -rf $(LCLLIBDIR)
	rm $(PRG)
